import React from 'react';

//Provider and Consumer
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;
export default UserContext;