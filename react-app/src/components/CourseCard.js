import {Button, Card} from 'react-bootstrap';
// import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
	// console.log(props);
	// console.log(typeof props);

	const { name, description, price, _id} = courseProp;
	// console.log(courseProp)

	/*
		Syntax:
			const [getter, setter] = useState(initialGetterValue)
	*/
	// const [count, setCount] = useState(0);
	// const [seats, setSeat] = useState(30);

	// function enroll() {
	// 	setSeat(seats - 1)
	// 	setCount(count + 1)
	// 	console.log('Counts' + count)
	// 	console.log('Seats' + seats)
	// }

	// useEffect(() => {

	// 	if(seats === 0) {
	// 		alert("No more seats availalbe.");
	// 	}

	// }, [seats]);


	return(
		<Card>
		  <Card.Body>
		    <Card.Title>{name}</Card.Title>
		    <Card.Subtitle>Description:</Card.Subtitle>
		    <Card.Text>
		      {description}
		    </Card.Text>
		    <Card.Subtitle>Price:</Card.Subtitle>
		    <Card.Text>
		      PhP {price}
		    </Card.Text>
{/*		    <Card.Text>
		       Enrollees: {count}
		       </Card.Text>
		       <Button variant="primary" onClick={enroll}>Enroll</Button>*/}
		  	<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
		  </Card.Body>
		</Card>
		)

}

