import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Register (){

	const { user } = useContext(UserContext)
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('')
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	//console.log(email);
	//console.log(password1);
	//console.log(password2);

	function registerUser(e) {
		console.log(e)

		e.preventDefault()

		// setEmail('');
		// setPassword1('');
		// setPassword2('');

		// alert('Thank you for registering!');

		/*
			ACTIVITY:
				Create a post request using 
				fetch to the /checkEmail endpoint of 
				our API that will check if the email
				the user is trying to submit is unique
				or not.

				If the email is unique, console.log 
				the API response.

				if the email is NOT unique, show a 
				sweetalert notification saying
				"Duplicate email found = Please try
				another email address"
		*/

		fetch("http://localhost:4000/users/checkEmail", {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res =>  res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please try another email address"
				})
				
			} else {
				// console.log(data);
				/*
					Create another fetch request upon receiving a false response from
					the previous request

					Send the request to the /register endpoint with all necessary
					information.

					If the registration is successful, show a sweetalert notification
					saying "Registration successfull - You may now log in"

					If not, show a sweetalert notification saying "Registration
					failed - Please try again"

				*/

				fetch("http://localhost:4000/users/register", {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(isSuccessful => {
					if(isSuccessful) {
						Swal.fire({
							title: "Registration successful",
							icon: "success",
							text: "You may now log in"
						})
					} else {
						Swal.fire({
							title: "Registration failed",
							icon: "error",
							text: "Please try again"
						})
					}
				})
			}
		})

		// setEmail('');
		// setPassword1('');
		// setPassword2('');

	}

	useEffect(() => {
		//no blank fields
		//mobile number must be exactly 11 characters
		//password must match
		if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (mobileNo.length === 11) && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false)
		}

	}, [firstName, lastName, email, mobileNo, password1, password2])



	return(
		
		(user.id !== null) ? 

			<Navigate to="/courses"/>
			
			:

			<Form className="mt-3" onSubmit = {(e) => registerUser(e)}>
			<h1>Register</h1>
			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter first name"
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter last name"
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control 
					type="email" 
					placeholder="Enter email"
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter mobile number"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Password"
					value={password1}
					onChange= {e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password2">
				<Form.Label> Verify Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Verify Password"
					value={password2}
					onChange={e=> setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			  {
			  	isActive ? 
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		  Submit
			  		</Button>
			  		:
			  		<Button variant="danger" type="submit" id="submitBtn" disabled>
			  		  Submit
			  		</Button>
			  }

			</Form>
		
		)

}

/*

	Mini-Activity:
		If the user goes to /register route (Registration Page) when the user is already logged in, navigate/redirect to the /courses route (Courses Page)

		6:30 PM

*/
